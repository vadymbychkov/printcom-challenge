import Vue from "vue";
import Vuex from "vuex";

import businesscards from '../../businesscards.json'
import flyers from '../../flyers.json'
import posters from '../../posters.json'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    selectedCategory: null,
    productProps: null
  },
  mutations: {
    setCategory(state, categoryData) {
      state.selectedCategory = categoryData
    },
    setProductProps(state, productProps) {
      state.productProps = productProps
    },
  },
  actions: {
    getCategory({commit}, categoryName) {
      switch (categoryName) {
        case 'businesscards':
          commit('setCategory', businesscards)
          break
        case 'flyers':
          commit('setCategory', flyers)
          break
        case 'posters':
          commit('setCategory', posters)
          break
        default:
          return
      }
    }
  }
});
